import torch
import numpy as np;
from torch.autograd import Variable
import scipy.io as sio

def normal_std(x):
    return x.std() * np.sqrt((len(x) - 1.)/(len(x)))

class Data_utility(object):
    # train and valid is the ratio of training set and validation set. test = 1 - train - valid
    def __init__(self, file_name, train, valid, cuda, horizon, window, normalize = 2):
        self.cuda = cuda
        self.P = window
        self.h = horizon
 #       fin = open(file_name);
 #       self.rawdat = np.loadtxt(fin, delimiter=',');
 #       self.dat = np.zeros(self.rawdat.shape);
        File = sio.loadmat(file_name)
        self.rawdat = File['SolarData']
        self.dat = self.rawdat
        self.d, self.n, self.m = self.dat.shape;
#        self.normalize = 2
        self.scale = np.ones(self.m);
#        self._normalized(normalize);
        self._split(int(train * self.d), int((train + valid) * self.d), self.d);
        
        self.scale = torch.from_numpy(self.scale).float();
        tmp = self.test[1] * self.scale.expand(self.test[1].size(0), self.m);
            
        if self.cuda:
            self.scale = self.scale.cuda();
        self.scale = Variable(self.scale);
        
        self.rse = normal_std(tmp);
        self.rae = torch.mean(torch.abs(tmp - torch.mean(tmp)));
    
#    def _normalized(self, normalize):
#        #normalized by the maximum value of entire matrix.
#       
#        if (normalize == 0):
#            self.dat = self.rawdat
#            
#        if (normalize == 1):
#            self.dat = self.rawdat / np.max(self.rawdat);
#            
#        #normlized by the maximum value of each row(sensor).
#        if (normalize == 2):
#            for i in range(self.m):
#                self.scale[i] = np.max(np.abs(self.rawdat[:,i]));
#                self.dat[:,i] = self.rawdat[:,i] / np.max(np.abs(self.rawdat[:,i]));
            
        
    def _split(self, train_day_end, valid_day_end, test_day_end):
        
        train_days = range(0, train_day_end)
        valid_days = range(train_day_end, valid_day_end)
        test_days = range(valid_day_end, test_day_end)
      
        self.train = self._batchify(train_days, self.h);
        self.valid = self._batchify(valid_days, self.h);
        self.test = self._batchify(test_days, self.h);
        
        
    def _batchify(self, daysRange, horizon):
        
        StartIndex = self.P + self.h - 1
        EndIndex = self.n - 1
        DailyDatNum = EndIndex - StartIndex + 1

        TotalDays = daysRange[-1] - daysRange[0] + 1        
        TotalData = TotalDays*DailyDatNum
        
        
#        print("TotalData=")
#        print(TotalData)
        
        X = torch.zeros((TotalData, self.P, self.m));
        Y = torch.zeros((TotalData, self.m));
        
        for i in daysRange:
            for j in range(StartIndex, EndIndex+1):
                
                ItemIndex = (i-daysRange[0])*DailyDatNum + j - StartIndex
#                print("ItemIndex=")
#                print(ItemIndex)
#                print("i=")
#                print(i)
#                print("i-P-h+1=")
#                print(j-self.P-self.h+1)
#                print("i-h+1=")
#                print(j-self.h+1)
                X[ItemIndex,:,:] = torch.from_numpy(self.dat[i, j-self.P- self.h+1:j - self.h + 1, :]);
                Y[ItemIndex, :] = torch.from_numpy(self.dat[i, j, :]);

        return [X, Y];

    def get_batches(self, inputs, targets, batch_size, shuffle=True):
        length = len(inputs)
        if shuffle:
            index = torch.randperm(length)
        else:
            index = torch.LongTensor(range(length))
        start_idx = 0
        while (start_idx < length):
            end_idx = min(length, start_idx + batch_size)
            excerpt = index[start_idx:end_idx]
            X = inputs[excerpt]; Y = targets[excerpt];
            if (self.cuda):
                X = X.cuda();
                Y = Y.cuda();  
            yield Variable(X), Variable(Y);
            start_idx += batch_size
